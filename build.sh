#!/usr/bin/env sh

set -e

ENV_NAME=${1}

SRC_PATH="src"
DIST_PATH="dist"
ENV_PATH="environments"

rm -rf ${DIST_PATH}
cp -r ${SRC_PATH} ${DIST_PATH}
cp "${ENV_PATH}/environment.${ENV_NAME}.json" "${DIST_PATH}/environment.json"
