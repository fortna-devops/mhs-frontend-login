let environmentObj;
let userPool;
let sessionUserAttributes;
let username;
let cognitoUser;
const bgId = 'loginPage';
const bgImageList = ['login-bg-1.jpg', 'login-bg-2.jpg', 'login-bg-3.jpg', 'login-bg-4.jpg'];

init();

function init() {
  setBackgroundImage(bgImageList);

  // TODO 
  loadJSON(function (response) {
    environmentObj = JSON.parse(response);
    const poolData = {
      UserPoolId: environmentObj.userPoolId,
      ClientId: environmentObj.clientId,
    }

    userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
  });

  const inputs = document.getElementsByClassName('form-input');
  const numInputs = inputs.length;

  for (var i = 0; i < numInputs; i++) {
    inputs[i].addEventListener(
      'keyup',
      (event) => {
        // 13 = "Enter" key
        if (event.keyCode === 13) {
          event.preventDefault();
          const form = event.target.closest('form');
          form.querySelector("button[type='button']").click();
        }
      },
      false
    );
  }
}

function signIn(form) {
  console.log('SignIn start');
  clearFormMessages('errorSignIn');
  clearFormMessages('success');
  validate(form.username.value, form.password.value);
}

function validate(username, password) {
  if (username.length !== 0 && password.length !== 0) {
    setFormMessage('111');
    authenticate(username, password).then(
      (res) => {
        console.log('Authenticated');
        // const accessToken = res.result.getAccessToken().getJwtToken();
      },
      (err) => {
        console.error('something wrong occurred: ' + err);
      }
    );
  } else {
    setFormMessage('Username and password are required', 'errorSignIn');
  }
}

function setFormMessage(message, id = 'error') {
  let p = document.createElement('p');
  document.getElementById(id).appendChild(p).innerText = message;
}

function clearFormMessages(id = 'error') {
  const node = document.getElementById(id);
  while (node && node.firstChild) {
    node.removeChild(node.firstChild);
  }
}

const onLoginSuccess = (res) => {
  const tokenDatetime = res['idToken'].payload.iat;
  if(isTimeOK(tokenDatetime)) {

    setCookie('accessToken', res['accessToken'].jwtToken, res['accessToken'].payload.exp);
    setCookie('refreshToken', res['refreshToken'].token);
    setCookie('token', res['idToken'].jwtToken, res['idToken'].payload.exp);

    const date = new Date(res['idToken'].payload.exp * 1000);
    setCookie('expires_at', date.toGMTString(), res['idToken'].payload.exp);

    localStorage.clear();
    window.location.href = '/';
  } else {
    showPopup();
  }
};

function isTimeOK() {
  return cognitoUser.getSession(function(err, result) {
      console.log('Clock Drift:')
      console.log(result.clockDrift )
          return !(result.clockDrift % 60 > 5)
  });
}

function millisToMinutesAndSeconds(millis) {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}

function setBackgroundImage(items) {
  const image = items[(items.length * Math.random()) | 0];
  document.getElementById(bgId).style.backgroundImage = 'url(images/' + image + ')';
}

function authenticate(username, password) {
  console.log('Starting the authentication');
  clearFormMessages();
  clearFormMessages('success');
  const authData = {
    Username: username,
    Password: password,
  };
  const authDetails = new AmazonCognitoIdentity.AuthenticationDetails(authData);

  const userData = {
    Username: username,
    Pool: userPool,
  };
  cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
  return new Promise((resolve, reject) => {
    cognitoUser.authenticateUser(authDetails, {
      onSuccess: (result) => {
        resolve(onLoginSuccess(result));
      },
      onFailure: (error) => reject(onLoginFailure(error.message)),
      newPasswordRequired: (userAttributes, requiredAttributes) => {
        // User was signed up by an admin and must provide new
        // password and required attributes, if any, to complete
        // authentication.

        // the api doesn't accept this field back
        delete userAttributes.email_verified;
        delete userAttributes.phone_number_verified;

        // store userAttributes on global variable
        sessionUserAttributes = userAttributes;

        document.getElementById('changePasswordForm').style.display = 'flex';
        document.getElementById('signInForm').style.display = 'none';

        localStorage.setItem('username', username);

        resolve({
          type: 'newPasswordRequired',
          result: [userAttributes, requiredAttributes],
        });
      },
    });
  });
}

function onLoginFailure(message) {
  setFormMessage(message, 'errorSignIn');
  setFormMessage('Please note that username and password are both case-sensitive.', 'errorSignIn');
}

function changePassword(form) {
  clearFormMessages();
  clearFormMessages('success');
  if (validatePassword(form)) {
    cognitoUser.completeNewPasswordChallenge(form.new_password.value, sessionUserAttributes, {
      onSuccess: (result) => {
        setFormMessage(
          'Your password changed succesfully. You will be redirected to the Sign In form',
          'success'
        );
        setTimeout(() => {
          window.location.reload();
        }, 3000);
      },
      onFailure: (err) => {
        console.log(err);
      },
    });
  }
}

function validatePassword(form, needConfirm = true) {
  re = /^\w+$/;
  const new_password = form.new_password.value;
  const confirm_password = needConfirm ? form.confirm_password.value : new_password;
  const username = localStorage.getItem('username');
  let verify = true;
  if (new_password !== '' && new_password === confirm_password) {
    if (new_password.length < 8) {
      setFormMessage('Password must contain at least 8 characters');
      form.new_password.focus();
      verify = false;
    }
    if (new_password === username) {
      setFormMessage('Password must be different from Username');
      form.new_password.focus();
      verify = false;
    }

    re = /[0-9]/;
    if (!re.test(new_password)) {
      setFormMessage('Password must contain at least one number (0-9)');
      form.new_password.focus();
      verify = false;
    }

    re = /[a-z]/;
    if (!re.test(new_password)) {
      setFormMessage('Password must contain a lower case letter');
      form.new_password.focus();
      verify = false;
    }

    re = /[A-Z]/;
    if (!re.test(new_password)) {
      setFormMessage('Password must contain an upper case letter');
      form.new_password.focus();
      verify = false;
    }

    re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if (!re.test(new_password)) {
      setFormMessage('Password must contain a special character');
      form.new_password.focus();
      verify = false;
    }

    return verify;
  } else {
    setFormMessage("Please check that you've entered and confirmed your password!");
    form.new_password.focus();
    verify = false;
    return verify;
  }
}

function sendCode(form) {
  clearFormMessages();
  clearFormMessages('success');
  if (form.username.value.length > 0) {
    const userData = {
      Username: form.username.value,
      Pool: userPool,
    };
    localStorage.setItem('username', form.username.value);
    const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.forgotPassword({
      onSuccess: function (result) {
        window.location.href = 'reset-password.html';
      },
      onFailure: function (err) {
        setFormMessage(err.message);
      },
    });
  } else {
    setFormMessage("Username can't be blank");
  }
}

function setNewPassword(form) {
  clearFormMessages();
  clearFormMessages('success');

  if (form.code.value.length > 0 && form.new_password.value.length > 0) {
    if (validatePassword(form, false)) {
      const userData = {
        Username: localStorage.getItem('username'),
        Pool: userPool,
      };
      const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
      const verificationCode = form.code.value;
      const newPassword = form.new_password.value;
      cognitoUser.confirmPassword(verificationCode, newPassword, {
        onSuccess: function (result) {
          localStorage.setItem('username', '');
          setFormMessage(
            'Your password changed succesfully. You will be redirected to the Sign In page',
            'success'
          );
          setTimeout(() => {
            window.location.href = 'index.html';
          }, 3000);
        },
        onFailure: function (err) {
          setFormMessage(err.message);
        },
      });
    }
  } else {
    setFormMessage("New password and code fields can't be blank");
  }
}

function setCookie(cookieName, cookieValue, exp, path = '/') {
  const date = new Date(exp * 1000);

  document.cookie =
    encodeURIComponent(cookieName) +
    '=' +
    encodeURIComponent(cookieValue) +
    '; path=' +
    path +
    '; expires=' +
    date.toGMTString();
}

function getCookie(cookieName) {
  const name = cookieName + '=';
  const allCookieArray = document.cookie.split(';');
  for (let i = 0; i < allCookieArray.length; i++) {
    let temp = allCookieArray[i].trim();
    if (temp.indexOf(name) == 0) return temp.substring(name.length, temp.length);
  }
  return '';
}

function eraseCookie(name) {
  document.cookie = name + '=; Max-Age=-99999999;';
}

function loadJSON(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType('application/json');
  xobj.open('GET', 'environment.json', true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == '200') {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}


function showPopup() {
  let popupContainer = document.getElementsByClassName("popup-container")[0];
  popupContainer.style.visibility = "visible";
  popupContainer.style.opacity = "1";
}

function hidePopup() {
  let popupContainer = document.getElementsByClassName("popup-container")[0];
  popupContainer.style.visibility = "hidden";
  popupContainer.style.opacity = "0";
}


